## Dependencies

All modules are available on PyPI:

 - `jax`
 - `numpy`
 - `scipy`
 - `parametrization_cookbook`
 - `matplotlib`

## Howto reproduce


### Reproduce figures

```
python3 plot1run.py
```

### Reproduce tables from intermediate results (pkl.gz files)

```
python3 build_res.py
```

### Reproduce intermediate results (generate the pkl.gz files)

Warning, this is time consuming. User may change the number of processor in the
`multiprocess.Pool` line.

```
python3 run_all_100.py
```

```
python3 run_all_200.py
```

## Use for custom net

E.g.: with a adjacency matrix `adjacency_matrix` and 5 groups:

```python

import model
import algos
import jax

obs = model.make_obs(adjacency_matrix)

prng_key = jax.random.PRNGKey(0)
result = algos.estim(5, obs, prng_key, retries=10, debug=True)
```
